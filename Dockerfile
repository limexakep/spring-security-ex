FROM openjdk:19-alpine
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
COPY src ./src
RUN dos2unix mvnw
RUN ./mvnw dependency:go-offline
CMD ["./mvnw", "spring-boot:run"]
