package equ.phoenix.simplejpa.exception;

import equ.phoenix.simplejpa.annotation.CustomAnnotation;
import equ.phoenix.simplejpa.controller.ClientController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = ClientController.class)
public class TestAdvice {
    @ExceptionHandler(value = {BusinessException.class})
    public ResponseEntity<Object> businessExceptionHandler(BusinessException ex) {
        return ResponseEntity
                .status(HttpStatus.I_AM_A_TEAPOT)
                .body(new ResponseMessage(ex.getMessage(), HttpStatus.I_AM_A_TEAPOT));
    }
}
