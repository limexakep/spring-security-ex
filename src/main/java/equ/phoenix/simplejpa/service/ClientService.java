package equ.phoenix.simplejpa.service;

import equ.phoenix.simplejpa.dto.RequestClientDto;
import equ.phoenix.simplejpa.entity.ClientEntity;
import equ.phoenix.simplejpa.model.Client;
import equ.phoenix.simplejpa.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository repository;

    public void createClient(RequestClientDto clientDto) {
      repository.save(
              new ClientEntity(
                      clientDto.getName(),
                      clientDto.getLastName(),
                      clientDto.getEmail(),
                      clientDto.getPassword()
              )
      );
    }
  /*
  Реализуйте CRUD-операции для клиента:
    •	Создание нового клиента
    •	Получение клиента по его id
    •	Получение всех клиентов
    •	Обновление клиента
    •	Удаление клиента

    Вы НЕ должны менять что-то в репозитории.
   */
}
