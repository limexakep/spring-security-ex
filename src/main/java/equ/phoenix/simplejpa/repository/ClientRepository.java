package equ.phoenix.simplejpa.repository;

import equ.phoenix.simplejpa.entity.ClientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends CrudRepository<ClientEntity, Long> {
    List<ClientEntity> getClientsByLastNameAndEmail(String lastName, String email);

    ClientEntity findClientEntityByEmailAndName(String email, String name);
}
