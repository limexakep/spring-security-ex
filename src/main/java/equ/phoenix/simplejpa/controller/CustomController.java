package equ.phoenix.simplejpa.controller;

import equ.phoenix.simplejpa.annotation.CustomAnnotation;
import equ.phoenix.simplejpa.exception.BusinessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class CustomController {
    @GetMapping
    public ResponseEntity<String> sayHelloTest() {
        throw new BusinessException("Test controller exception");
        //return ResponseEntity.;
    }
}
