package equ.phoenix.simplejpa.controller;

import equ.phoenix.simplejpa.annotation.CustomAnnotation;
import equ.phoenix.simplejpa.dto.RequestClientDto;
import equ.phoenix.simplejpa.exception.BusinessException;
import equ.phoenix.simplejpa.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CustomAnnotation
public class ClientController {
    private static Logger logger = LoggerFactory.getLogger(ClientController.class);
    @Autowired
    ClientService clientService;

    @PostMapping("/")
    void createClient(@RequestBody RequestClientDto clientDto) {
        clientService.createClient(clientDto);
    }

    @GetMapping("/user")
    String sayHello(HttpServletRequest request) {
        return "Hello " + request.getUserPrincipal().getName();
    }

    @GetMapping("/admin")
    String sayHelloAdmin() {
        return "Hello, admin!";
    }

    @GetMapping("/all")
    ResponseEntity<Object> sayHelloAll(
            @RequestParam(required = false, defaultValue = "false")
            boolean error) {
        if (error) {
            throw new BusinessException("Какая-то ошибка!");
        } else {
            logger.error("Error message!");
            logger.warn("Warn message!");
            logger.info("Info message!");
            logger.debug("Debug message!");
            logger.trace("Trace message!");
            return ResponseEntity.ok("Hello, all!");
        }
    }
}
